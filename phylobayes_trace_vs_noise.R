




w_d16o_cteno_data = read.table("~/git/sitelogl/chains/whelan-d16o-cteno-1.trace", header=TRUE, sep="\t")
w_d16o_pori_data = read.table("~/git/sitelogl/chains/whelan-d16o-pori-1.trace", header=TRUE, sep="\t")
w_d16o_para_data = read.table("~/git/sitelogl/chains/whelan-d16o-paranimalia-1.trace", header=TRUE, sep="\t")

logln_cteno = w_d16o_cteno_data[,4]
logln_pori = w_d16o_pori_data[,4]
logln_para = w_d16o_para_data[,4]

ctenomean = mean(logln_cteno[100:length(logln_cteno)])
porimean = mean(logln_pori[100:length(logln_pori)])
paramean = mean(logln_para[100:length(logln_para)])

ctenodelta = diff(range(logln_cteno[100:length(logln_cteno)]) )
poridelta = diff(range(logln_pori[100:length(logln_pori)]) )
paradelta = diff(range(logln_para[100:length(logln_para)]) )

ct_po_diff = abs(mean(logln_cteno[100:length(logln_cteno)]) - mean(logln_pori[100:length(logln_pori)]) )

num_iter = max( w_d16o_cteno_data[,1], w_d16o_pori_data[,1], w_d16o_para_data[,1] )
max_lnl = max( w_d16o_cteno_data[,4], w_d16o_pori_data[,4], w_d16o_para_data[,4] )


pdf(file="~/git/sitelogl/chain_plots/whelan-d16o_lnl_trace.pdf", height=7, width=8)
plot(0,0, type='n', xlim=c(100,num_iter), ylim=c(1.01,0.99)*max_lnl, cex.lab=1.3, cex.axis=1.3, xlab="Iter", ylab="logL" )
lines( w_d16o_cteno_data[,1], logln_cteno, lwd=3, col="#f1a34099"  )
lines( w_d16o_pori_data[,1], logln_pori, lwd=3, col="#0571b099"  )
lines( w_d16o_para_data[,1], logln_para, lwd=3, col="#4daf4a99"  )

legend_text = c("Cteno-sis", "Pori-sis", "Paranimalia")

legend( 100, max_lnl*0.991, legend=legend_text, text.col=c("#f1a340","#0571b0","#4daf4a"), cex=2, bty='n', title="Tree",title.col="black" )

delta_legend = c(ctenodelta, poridelta, paradelta)
legend( mean(c(100,num_iter)), max_lnl*0.991, legend=delta_legend, text.col=c("#f1a340","#0571b0","#4daf4a"), cex=2, bty='n', title=expression(Delta),title.col="black" )

text( mean(c(100,num_iter)), max_lnl*1.009, paste("Cteno-Pori =", round(ct_po_diff,digits=3)), cex=1.3)

dev.off()